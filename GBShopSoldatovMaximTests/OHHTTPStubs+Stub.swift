//
//  OHHTTPStubs+Stub.swift
//  GBShopSoldatovMaximTests
//
//  Created by Maxim Soldatov on 02/11/2018.
//  Copyright © 2018 Maxim Soldatov. All rights reserved.
//

import Foundation
import OHHTTPStubs


enum StubMethod {
    case get
    case post
    case put
    case patch
    case delete
}



/// Удаляет все стабы
func removeAllHTTPStubs() {
    OHHTTPStubs.removeAllStubs()
}

/// Хелпер для включения моков сетевых запросов
///
/// - Parameters:
///   - method: Метод запроса
///   - fileName: Имя файла с json данными, без расширения
///   - path: Путь метода на сервере
///   - statusCode: Статус, с которым будет завершен запрос
func stubHTTPResponse(
    method: StubMethod,
    fileName: String,
    path: String,
    statusCode: Int32,
    header: [String: String] = [:]) {
    
    
    let fileURL = Bundle(for: GBShopSoldatovMaximTests.self).url(forResource: fileName, withExtension: "json")!
    
    
    let conditionMethod: OHHTTPStubsTestBlock
    switch method {
    case .get:
        conditionMethod = isMethodGET()
    case .post:
        conditionMethod = isMethodPOST()
    case .put:
        conditionMethod = isMethodPUT()
    case .patch:
        conditionMethod = isMethodPATCH()
    case .delete:
        conditionMethod = isMethodDELETE()
    }
    stub(condition: conditionMethod && pathEndsWith(path)) { _ in
        return OHHTTPStubsResponse(
            fileURL: fileURL,
            statusCode: statusCode,
            headers: header
        )
    }
}
