//
//  GBShopSoldatovMaximTests.swift
//  GBShopSoldatovMaximTests
//
//  Created by Maxim Soldatov on 24/10/2018.
//  Copyright © 2018 Maxim Soldatov. All rights reserved.
//

import XCTest
import Alamofire
import OHHTTPStubs

@testable import GBShopSoldatovMaxim

struct SignInResponseStub: Decodable {
    
    let result: Int
    let authToken: String
    
    let id_user: Int
    let user_login: String
    let user_name: String
    let user_lastname: String
    
    private enum SignInResponseKeys: String, CodingKey {
        case result
        case user
        case authToken
    }
    
    private enum UserKeys: String, CodingKey {
        case id_user
        case user_login
        case user_name
        case lastName = "user_lastname"
    }
    
    init(from decoder: Decoder) throws {
        
        let responseContainer = try decoder.container(keyedBy: SignInResponseKeys.self)
        
        self.result = try responseContainer.decode(Int.self, forKey: .result)
        self.authToken = try responseContainer.decode(String.self, forKey: .authToken)
        
        let userContainer = try responseContainer.nestedContainer(keyedBy: UserKeys.self, forKey: .user)
        
        self.id_user = try userContainer.decode(Int.self, forKey: .id_user)
        self.user_login = try userContainer.decode(String.self, forKey: .user_login)
        self.user_name = try userContainer.decode(String.self, forKey: .user_name)
        self.user_lastname = try userContainer.decode(String.self, forKey: .lastName)
    }
}


enum ApiErrorStub: Error {
    case fatalError
}

struct ErrorParserMock: AbstractErrorParser {
    
    var error: AppError?
    
    func parse(_ result: Error) -> AppError {
        return error ?? .udefinedError
    }
    
    func parse(response: HTTPURLResponse?, data: Data?, error: Error?) -> AppError? {
        
        return self.error
    }
    
    
    
}

class GBShopSoldatovMaximTests: XCTestCase {

    var errorParser: ErrorParserMock!
    var sessionManager: SessionManager!
    
    override func setUp() {
        errorParser = ErrorParserMock()
        let config = URLSessionConfiguration.ephemeral
        OHHTTPStubs.setEnabled(true, for: config)
        sessionManager = SessionManager(configuration: config)
        
        super.setUp()
    }

    override func tearDown() {
    OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }

    func testShouldSendRequestAndParse() {
        
        let expectation = self.expectation(description: "failyre")
        errorParser.error = AppError.serverError
        var error: Error?
 
        sessionManager
            .request("https://jsonplaceholder.typicode.com/posts/1")
            .responseCodable(errorParser: errorParser) {(response: DataResponse<SignInResponseStub>) in
                
                error = response.result.error
                expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
        XCTAssertNotNil(error)
    }
    
    
    func testShouldSendRequestAndParseWithError() {
        
        let expectation = self.expectation(description: "success")

        stubHTTPResponse(
            method: .get,
            fileName: "SignInStub",
            path: "/posts/1",
            statusCode: 200
        )
        
        sessionManager
            .request("https://jsonplaceholder.typicode.com/posts/1")
            .responseCodable(errorParser: errorParser) {(response: DataResponse<SignInResponseStub>) in
                
                switch response.result {
                case .success(_):break
                case .failure:
                    XCTFail()
                }
                expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
        
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
