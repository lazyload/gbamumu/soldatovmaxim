//
//  AbstractRequestFactory.swift
//  GBShopSoldatovMaxim
//
//  Created by Maxim Soldatov on 28/10/2018.
//  Copyright © 2018 Maxim Soldatov. All rights reserved.
//

import Foundation

import Alamofire

protocol AbstractRequestFatory: class {
    var errorParser: AbstractErrorParser { get }
    var errorHadler: AbstractErrorHandler { get }
    var sessionManager: SessionManager { get }
    var queue: DispatchQueue? { get }
    
    @discardableResult
    func request<T: Decodable>(
        reques: URLRequestConvertible,
        completionHandler: @escaping (T) -> Void)
        -> DataRequest
}

extension AbstractRequestFatory {
    
    @discardableResult
    public func request<T: Decodable>(
        reques: URLRequestConvertible,
        completionHandler: @escaping (T) -> Void)
        -> DataRequest {
            return sessionManager
                .request(reques)
                .responseCodable(errorParser: errorParser, queue: queue) { [weak self] (response: DataResponse<T>) in
                  
                    switch response.result {
                    case .success(let value):
                        completionHandler(value)
                    case .failure(let error):
                        self?.errorHadler.handle(error: error as! AppError)
                    }
            }
    }
}
