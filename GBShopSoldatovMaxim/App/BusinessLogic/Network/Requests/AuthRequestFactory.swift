//
//  RequestFactory.swift
//  GBShopSoldatovMaxim
//
//  Created by Maxim Soldatov on 28/10/2018.
//  Copyright © 2018 Maxim Soldatov. All rights reserved.
//

import Foundation
import Alamofire


protocol AuthRequestFactory {
    
    func sendSignInParametr(username: String, password: String, completion: @escaping (SignInResponse) -> Void)
    
     func sendSignOutParametr(userId: String, completion: @escaping (SignOutResponse) -> Void)
}


class AuthRequestFactoryImp: AuthRequestFactory, AbstractRequestFatory {
    
    

    let errorParser: AbstractErrorParser
    let errorHadler: AbstractErrorHandler
    let sessionManager: SessionManager
    let queue: DispatchQueue?
    let baseUrl = URL(string: "https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/")!
    
    init(
        errorParser: AbstractErrorParser,
        errorHadler: AbstractErrorHandler,
        sessionManager: SessionManager,
        queue: DispatchQueue? = DispatchQueue.global()) {
        
        self.errorParser = errorParser
        self.errorHadler = errorHadler
        self.sessionManager = sessionManager
        self.queue = queue
    }
    
    func sendSignInParametr( username: String, password: String, completion: @escaping (SignInResponse) -> Void) {
        let route = SignIn(baseUrl: baseUrl, login: username, password: password)
        request(reques: route, completionHandler: completion)
        
    }
    
    func sendSignOutParametr(userId: String, completion: @escaping (SignOutResponse) -> Void) {
        let route = SignOut(baseUrl: baseUrl, userId: userId)
        request(reques: route, completionHandler: completion)
    }
}



extension AuthRequestFactoryImp {
   
    struct SignIn: RequestRouter {
        
        let baseUrl: URL
        let method: HTTPMethod = .get
        let path: String = "login.json"
        
        let login: String
        let password: String
        var parameters: Parameters? {
            return [
                "username": login,
                "password": password
            ]
        }
    }
    
    struct SignOut: RequestRouter {
        
        let baseUrl: URL
        let method: HTTPMethod = .get
        let path: String = "logout.json"
        
        let userId: String
        var parameters: Parameters? {
            return [
                "id_user": userId,
            ]
        }
    }
}

