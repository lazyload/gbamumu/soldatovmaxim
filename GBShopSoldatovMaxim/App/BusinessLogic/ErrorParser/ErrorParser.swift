//
//  ErrorParser.swift
//  GBShopSoldatovMaxim
//
//  Created by Maxim Soldatov on 28/10/2018.
//  Copyright © 2018 Maxim Soldatov. All rights reserved.
//

import Foundation

enum AppError: Error {
    case serverError
    case networkError
    case udefinedError
}

protocol AbstractErrorParser {
    func parse(_ result: Error) -> AppError
    func parse(response: HTTPURLResponse?, data: Data?, error: Error?) -> AppError?
}

class ErrorParser: AbstractErrorParser {
    func parse(_ result: Error) -> AppError {
        if result is DecodingError {
            return AppError.serverError
        }
        
        if let error = result as NSError?, error.code == -1001 || error.code == -1009 {
            return AppError.networkError
        }
        
        return .udefinedError
    }
    
    func parse(response: HTTPURLResponse?, data: Data?, error: Error?) -> AppError? {
        return error != nil ? .udefinedError : nil
    }
}

