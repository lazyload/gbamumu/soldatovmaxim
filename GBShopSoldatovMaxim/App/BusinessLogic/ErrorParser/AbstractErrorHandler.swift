//
//  AbstractErrorHandler.swift
//  GBShopSoldatovMaxim
//
//  Created by Maxim Soldatov on 30/10/2018.
//  Copyright © 2018 Maxim Soldatov. All rights reserved.
//

import Foundation

protocol AbstractErrorHandler {
    
    func handle(error: AppError)
}


final class PrintErrorHandler: AbstractErrorHandler {
    func handle(error: AppError) {
        print(error)
    }
    
    
}
