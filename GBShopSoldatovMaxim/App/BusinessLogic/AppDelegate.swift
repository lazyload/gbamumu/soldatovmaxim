
//  AppDelegate.swift
//  GBShopSoldatovMaxim
//
//  Created by Maxim Soldatov on 24/10/2018.
//  Copyright © 2018 Maxim Soldatov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    let factory = di.resolve(AuthRequestFactory.self)!
   
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
      
        factory.sendSignInParametr(username: "Somebody", password: "mypassword") { [weak self] (user) in
            print(user)
        }
        
        factory.sendSignOutParametr(userId: "123") { [weak self] (success) in
            print(success)
        }
        return true
    }
}

