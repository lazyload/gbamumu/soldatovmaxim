//
//  SignInViewController.swift
//  GBShopSoldatovMaxim
//
//  Created by Maxim Soldatov on 28/10/2018.
//  Copyright © 2018 Maxim Soldatov. All rights reserved.
//

import UIKit
import Alamofire

class SignInViewController: UIViewController {

    let factory = di.resolve(AuthRequestFactory.self)!
    
    var user: SignInResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.red
        
        factory.sendSignInParametr(username: "Somebody", password: "mypassword") { [weak self] (user) in
        
                self?.user = user
                print(user)
            }
        }
    }

