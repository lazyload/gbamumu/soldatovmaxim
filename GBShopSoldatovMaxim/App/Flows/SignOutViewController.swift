//
//  SignOutViewController.swift
//  GBShopSoldatovMaxim
//
//  Created by Maxim Soldatov on 28/10/2018.
//  Copyright © 2018 Maxim Soldatov. All rights reserved.
//

import UIKit
import Alamofire

class SignOutViewController: UIViewController {
    
    let factory = di.resolve(AuthRequestFactory.self)!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        factory.sendSignOutParametr(userId: "123") { [weak self] (response) in
            
            
            if response.result == 1 {
                print("Переход на экран авторизации")
            }
        }
    }
}


