//
//  NetworkAssembly.swift
//  GBShopSoldatovMaxim
//
//  Created by Maxim Soldatov on 30/10/2018.
//  Copyright © 2018 Maxim Soldatov. All rights reserved.
//


import Alamofire
import Foundation
import Swinject


// swiftlint:disable force_unwrapping function_body_length
final class NetworkAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.register(AbstractErrorParser.self) { _ in
            return ErrorParser()
            }.inObjectScope(.container)
        
        container.register(AbstractErrorHandler.self) { _ in
            return PrintErrorHandler()
            }.inObjectScope(.container)
        
        container.register(SessionManager.self) { _ in
            return SessionManager.default
            }.inObjectScope(.container)
        
        container.register(AuthRequestFactory.self) { resolver in
            return AuthRequestFactoryImp(
                errorParser: resolver.resolve(AbstractErrorParser.self)!,
                errorHadler: resolver.resolve(AbstractErrorHandler.self)!,
                sessionManager: resolver.resolve(SessionManager.self)!
            )
        }
    }
}
