//
//  AssemblyFactory.swift
//  GBShopSoldatovMaxim
//
//  Created by Maxim Soldatov on 30/10/2018.
//  Copyright © 2018 Maxim Soldatov. All rights reserved.
//

import Foundation
import Swinject

let di = AssemblyFactory().makeResolver()

/// Сборщик swinject контейнеров
/// Для корректной работы DI такой контейнер должен быть один на все приложение
struct AssemblyFactory {
    
    func makeResolver() -> Resolver {
        
        let assembly = Assembler([NetworkAssembly()])
        
        return assembly.resolver
    }
}
