//
//  SignOutResponse.swift
//  GBShopSoldatovMaxim
//
//  Created by Maxim Soldatov on 28/10/2018.
//  Copyright © 2018 Maxim Soldatov. All rights reserved.
//

import Foundation

struct SignOutResponse: Decodable {
    let result: Int
    
    private enum SignOutResponseKeys: String, CodingKey {
        case result
    }
    
    init(from decoder: Decoder) throws {
        
        let responseContainer = try decoder.container(keyedBy: SignOutResponseKeys.self)
        self.result = try responseContainer.decode(Int.self, forKey: .result)
    }
}
