//
//  SignInResponse.swift
//  GBShopSoldatovMaxim
//
//  Created by Maxim Soldatov on 28/10/2018.
//  Copyright © 2018 Maxim Soldatov. All rights reserved.
//

import Foundation

struct SignInResponse: Decodable {
    let result: Int
    let authToken: String
    
    let id_user: Int
    let user_login: String
    let user_name: String
    let user_lastname: String
    
    private enum SignInResponseKeys: String, CodingKey {
        case result
        case user
        case authToken
    }
    
    private enum UserKeys: String, CodingKey {
        case id_user
        case user_login
        case user_name
        case lastName = "user_lastname"
    }
    
    init(from decoder: Decoder) throws {
        
        let responseContainer = try decoder.container(keyedBy: SignInResponseKeys.self)
        
        self.result = try responseContainer.decode(Int.self, forKey: .result)
        self.authToken = try responseContainer.decode(String.self, forKey: .authToken)
        
        let userContainer = try responseContainer.nestedContainer(keyedBy: UserKeys.self, forKey: .user)
        
        self.id_user = try userContainer.decode(Int.self, forKey: .id_user)
        self.user_login = try userContainer.decode(String.self, forKey: .user_login)
        self.user_name = try userContainer.decode(String.self, forKey: .user_name)
        self.user_lastname = try userContainer.decode(String.self, forKey: .lastName)
    }
}



